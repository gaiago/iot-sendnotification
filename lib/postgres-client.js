const createClient = require('serverless-pg');

module.exports.PgClient = function PgClient () {

  const dbClient = createClient({
    config: {
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      user: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
    },
    onConnect: () => {},
    onClose: () => {},
    beforeQuery: () => {},
    afterQuery: () => {},
  });

  this.getCommunityName = async (tripGuid) => {
    let community = undefined
    try{
      community = await dbClient.query(`
      SELECT c2."ID", c2."Name" 
      FROM "Trips" t 
        JOIN "Users" u 
        ON t."UserID" = u."ID" 
        JOIN "UserGroup" ug 
        ON ug."UserID" = u."ID" 
        JOIN "CommunityGroup" cg 
        ON cg."GroupID" = ug."GroupID" 
        JOIN "Communities" c2 
        ON c2."ID" = cg."CommunityID" 
      WHERE t."Guid" =  '${tripGuid}'
      `);
    } catch (err) {
      throw new Error('Cannot retrieve community name for trip: ' + tripGuid)
    }
    return community.rows[0]
  }

  this.getStatsByCommunityName = async (communityName) => {
    let stats = undefined
    try {
      stats = await dbClient.query(`
      SELECT 
          COUNT(*) AS "TotalTrips",
          SUM("Trips"."ActualDurationSeconds") AS "TotalSeconds",
          SUM("Trips"."ActualMileageKm") AS "TotalKm"
      FROM "Trips" INNER JOIN "Users" ON ("Trips"."UserID" = "Users"."ID")
          INNER JOIN "UserGroup" ON ("UserGroup"."UserID" = "Users"."ID")
          INNER JOIN "CommunityGroup" ON ("UserGroup"."GroupID" = "CommunityGroup"."GroupID")
          INNER JOIN "Communities" ON ("Communities"."ID" = "CommunityGroup"."CommunityID")
      WHERE "Communities"."Name" = '` + communityName + `' "Trips"."IsCompleted" = '1'
    `);
    } catch (err) {
      throw new Error('Cannot retrieve stats for community: ' + communityName)
    }
    return stats.rows[0]
  }

  this.getStatsByCommunityId = async (communityId) => {
    let stats = undefined
    try {
      stats = await dbClient.query(`
      SELECT 
          COUNT(*) AS "TotalTrips",
          SUM("Trips"."ActualDurationSeconds") AS "TotalSeconds",
          SUM("Trips"."ActualMileageKm") AS "TotalKm"
      FROM "Trips" INNER JOIN "Users" ON ("Trips"."UserID" = "Users"."ID")
          INNER JOIN "UserGroup" ON ("UserGroup"."UserID" = "Users"."ID")
          INNER JOIN "CommunityGroup" ON ("UserGroup"."GroupID" = "CommunityGroup"."GroupID")
          INNER JOIN "Communities" ON ("Communities"."ID" = "CommunityGroup"."CommunityID")
      WHERE "Communities"."ID" = '` + communityId + `' AND "Trips"."IsCompleted" = '1'
    `);
    } catch (err) {
      throw new Error('Cannot retrieve stats for communityID: ' + communityId)
    }
    return stats.rows[0]
  }

  this.getGlobalStats = async () => {
    let stats = undefined
    try {
      stats = await dbClient.query(`
      SELECT 
          COUNT(*) AS "TotalTrips",
          SUM("Trips"."ActualDurationSeconds") AS "TotalSeconds",
          SUM("Trips"."ActualMileageKm") AS "TotalKm"
      FROM "Trips"
      WHERE "Trips"."IsCompleted" = '1'
    `);
    } catch (err) {
      throw new Error('Cannot retrieve global statistics')
    }
    return stats.rows[0]
  }
}