'use strict'

module.exports.success = (data) => {
  
  return {statusCode: 200,
    isBase64Encoded: false,
    multiValueHeaders: {
      'Content-Type': ['application/json'],
    },
    body: JSON.stringify(data)
  }
}

module.exports.error = (err, data, code = 500) => {
  console.log(err)
  return {statusCode: code,
    isBase64Encoded: false,
    multiValueHeaders: {
      'Content-Type': ['application/json'],
    },
    body: JSON.stringify(data)
  }
}