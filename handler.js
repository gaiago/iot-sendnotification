'use strict'

var { PgClient } = require('./lib/postgres-client');
const { success, error } = require('./lib/response')

var AWS = require('aws-sdk');
const IOT_ENDPOINT = process.env.IOT_ENDPOINT

module.exports.iotPublish = async (event, context, callback) => {
 const iotdata = new AWS.IotData({endpoint: IOT_ENDPOINT});
 const pushMessages = event.Records
 const DBClient = new PgClient()

 for(const element of pushMessages) {
    let snsBody = element.Sns
    let message = JSON.parse(snsBody.Message)
    // console.log(message)
    // const communityId = message.reservation.user.community.id
    // const communityName = message.reservation.user.community.name

    let globalStats = await DBClient.getGlobalStats()
    // let communityStats = await DBClient.getStatsByCommunityId(communityId)
    globalStats.TotalMinutes = Math.floor(globalStats.TotalSeconds / 60).toString()
    // communityStats.TotalMinutes = Math.floor(communityStats.TotalSeconds / 60).toString()
  
    var paramsGlobal = {
        topic: 'community/trips',
        payload: JSON.stringify(globalStats),
        qos: 0
    };
    iotdata.publish(paramsGlobal, function(err, data) {
        err ? console.log(err) : console.log("SENT >> " + JSON.stringify(paramsGlobal))
    });

    // var paramsCommunity = {
    //     topic: 'community/' + communityName + '/trips',
    //     payload: JSON.stringify(communityStats),
    //     qos: 0
    // };
    // iotdata.publish(paramsCommunity, function(err, data) {
    //     err ? console.log(err) : console.log("SENT >> " + JSON.stringify(paramsCommunity))
    // });
    }
 return;
}
module.exports.getStats = async (event, context, callback) => {
   const DBClient = new PgClient()
   let globalStats = await DBClient.getGlobalStats()
   globalStats.TotalMinutes = Math.floor(globalStats.TotalSeconds / 60).toString()
   
   return success(globalStats)
}