# StaticContentApi

## Prerequisites

- NodeJs 12
- Java

### Setup

- `npm install`


### Deploy Locally
Run locally database with preprod environment

In another shell run:
```
npm start
```

### Deploy to a specific environment
```
AWS_PROFILE=gaiago npm run deploy -- --env [preprod|prod]
```

> **WARNING** When you deploy Lambda to AWS, DynamoDB table will be created but not populated with seed data! You have to manually upload the key-value list

### Create Custom Domain (only first time)
```
SLS_DEBUG=* AWS_PROFILE=gaiago npm run deploy:domain -- --env [preprod|prod]
```
